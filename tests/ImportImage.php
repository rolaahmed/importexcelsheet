<?php

require __DIR__ . '/../autoload.php';

$objPHPExcel = PHPExcel_IOFactory::load("files/Candy Land - NEW ITEMS LIST.xlsx");

foreach ($objPHPExcel->getSheetByName("My Sheet")->getDrawingCollection() as $drawing) {
    if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing) {
        ob_start();
        call_user_func(
            $drawing->getRenderingFunction(),
            $drawing->getImageResource()
        );
        $imageContents = ob_get_contents();
        ob_end_clean();
    }
}
?>